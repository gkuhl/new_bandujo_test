Welcome to the Bandujo Web Coding test

This scaffold uses gulp and bower to manage dependencies and bootstrap for responsive layouts. Your scripts will go in app>scripts>main.js, and your styles will go in app>styles>main.scss. You will need to run `npm install` to install the dependencies. you can use `gulp serve` to run the dev server with autorefresh while you work, and `gulp build` to build a package for release.

The assets for this test can be downloaded here: https://www.dropbox.com/s/out7k655vutwx2w/bandujo_dev_test.zip?dl=0



